//
//  SecondViewController.swift
//  MyUtilities
//
//  Created by Dejan Novak on 03/04/2019.
//  Copyright © 2019 CROZ. All rights reserved.
//

import UIKit
import MapKit

class MyMapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, UISearchBarDelegate {

    @IBOutlet weak var mapView: MKMapView!
    var locationManager: CLLocationManager?
    var searchController: UISearchController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func currentLocationButtonAction(sender: UIBarButtonItem) {
        if (CLLocationManager.locationServicesEnabled()) {
            if locationManager == nil {
                locationManager = CLLocationManager()
            }
            locationManager?.requestWhenInUseAuthorization()
            locationManager?.delegate = self
            locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            locationManager?.requestAlwaysAuthorization()
            locationManager?.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        self.mapView?.setRegion(region, animated: true)
        if self.mapView?.annotations.count != 0 {
            let annotation = self.mapView?.annotations[0]
            self.mapView?.removeAnnotation(annotation!)
        }
        let pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = location!.coordinate
        pointAnnotation.title = "Moja lokacija"
        mapView?.addAnnotation(pointAnnotation)
    }
    
    
    @IBAction func searchButtonAction(button: UIBarButtonItem) {
        if searchController == nil {
            searchController = UISearchController(searchResultsController: nil)
        }
        searchController!.hidesNavigationBarDuringPresentation = false
        self.searchController!.searchBar.delegate = self
        present(searchController!, animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        locationManager?.stopUpdatingLocation()
        searchBar.resignFirstResponder()
        dismiss(animated: true, completion: nil)
        
        if self.mapView?.annotations.count != 0 {
            let annotation = self.mapView?.annotations[0]
            self.mapView?.removeAnnotation(annotation!)
        }
        
        let localSearchRequest = MKLocalSearchRequest()
        localSearchRequest.naturalLanguageQuery = searchBar.text
        let localSearch = MKLocalSearch(request: localSearchRequest)
        
        localSearch.start { [weak self]  (localSearchResponse, error) -> Void in
            if localSearchResponse == nil {
                let alertNoResults = UIAlertController(title: "Nema rezultata", message: "Nema rezultata", preferredStyle: .alert)
                alertNoResults.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                    return
                }))
                self?.present(alertNoResults, animated: true, completion: nil)
                return
            }
            let center = CLLocationCoordinate2D(latitude:     localSearchResponse!.boundingRegion.center.latitude, longitude: localSearchResponse!.boundingRegion.center.longitude)
            
            let pointAnnotation = MKPointAnnotation()
            pointAnnotation.title = searchBar.text
            pointAnnotation.coordinate = center
            
            self?.mapView?.addAnnotation(pointAnnotation)
            
            
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            self?.mapView?.setRegion(region, animated: true)
            
        }
    }



}

