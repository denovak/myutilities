//
//  NotesModel.swift
//  MyUtilities
//
//  Created by Dejan Novak on 08/04/2019.
//  Copyright © 2019 CROZ. All rights reserved.
//

import Foundation
import Alamofire

extension Notification.Name {
    static let didReloadData = Notification.Name("didReloadData")
}

class NotesModel {
    
    var noteArray: [ [String: Any] ] = []
    let serverAddress = "http://172.16.248.25:8080/api/notes"
    
    let headers: HTTPHeaders = [
        "content-type": "application/json",
        "Accept": "application/json"
    ]
    
    func saveNoteToBackend(indexOfNote: Int) {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let myDateAsString = formatter.string(from: Date())
        
        var note = noteArray[indexOfNote]
        note["noteDate"] = myDateAsString
        
        var request = Alamofire.request(serverAddress, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers).request
        
        request?.httpBody = try! JSONSerialization.data(withJSONObject: note , options: JSONSerialization.WritingOptions.prettyPrinted)
        
        Alamofire.request(request!).response { response in
            if response.response?.statusCode == 200 {
                self.getNotesFromBackend()
            }
            else {
                // TODO house keeping logic
            }

        }
    }
    
    func getNotesFromBackend() {
        Alamofire.request(serverAddress, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                if JSONSerialization.isValidJSONObject(response.result.value!) {
                    let myJson = try! JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [[String: Any]]
                    self.noteArray = myJson!
                    NotificationCenter.default.post(name: .didReloadData, object: nil)
                }
                else {
                    // TODO HOUSE KEEPING LOGIC
                }
        }
    }
    
    
    func deleteOrUpdateNoteFromBackend(noteIndex: Int, update: Bool) {
        
        let note = noteArray[noteIndex]
        let noteId = note["id"] as! Int
        
        let deleteOrUpdatePath = serverAddress + "/\(noteId)"
        var method: Alamofire.HTTPMethod?
        if update {
            method = Alamofire.HTTPMethod.put
        }
        else {
            method = Alamofire.HTTPMethod.delete
        }
        
        Alamofire.request(deleteOrUpdatePath, method: method!, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            .response { response in
                if response.response?.statusCode == 200 {
                    //print(response)
                    self.getNotesFromBackend()
                }
                else {
                    // TODO housekeeping logic
                    return
                }
        }
    }
    
   
    
}
