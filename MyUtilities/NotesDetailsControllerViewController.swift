//
//  NotesDetailsControllerViewController.swift
//  MyUtilities
//
//  Created by Dejan Novak on 04/04/2019.
//  Copyright © 2019 CROZ. All rights reserved.
//

import UIKit

class NotesDetailsControllerViewController: UIViewController {

    
    @IBOutlet weak var descriptionTextView: UITextView?
    @IBOutlet weak var titleTextField: UITextField?
    var noteIndex: Int?
    let applicationDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBAction func saveNote(sender: Any?) {
       
        let noteDescription = descriptionTextView?.text
        let noteTitle = titleTextField?.text
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let myDateAsString = formatter.string(from: Date())
        
        
        if let unwrappedIndex = noteIndex {
            var note: [String:Any] = applicationDelegate.notesModel.noteArray[unwrappedIndex]
    
            note["noteDescription"] = noteDescription
            note["noteTitle"] = noteTitle
            note["noteDate"] = myDateAsString
            applicationDelegate.notesModel.noteArray[unwrappedIndex] = note
            applicationDelegate.notesModel.deleteOrUpdateNoteFromBackend(noteIndex: unwrappedIndex, update: true)
        }
        else {
            let note: [String:Any] = ["noteDescription": noteDescription as! String, "noteTitle": noteTitle as! String, "noteDate": myDateAsString ]
            applicationDelegate.notesModel.noteArray.append(note)
            noteIndex = applicationDelegate.notesModel.noteArray.count-1
            applicationDelegate.notesModel.saveNoteToBackend(indexOfNote: noteIndex!)
        }
        
        self.navigationController?.popViewController(animated: true)
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let unwrappedIndex = noteIndex  {
            let note = applicationDelegate.notesModel.noteArray[unwrappedIndex]
            descriptionTextView?.text = note["noteDescription"] as? String
            titleTextField?.text = note["noteTitle"] as? String
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}
