//
//  Calculator.swift
//  MyUtilities
//
//  Created by Dejan Novak on 03/04/2019.
//  Copyright © 2019 CROZ. All rights reserved.
//

import Foundation

struct Calculator {
    
    var expression: [String] = []
    mutating func addOperand(operand: String) {
        if let _ = Double(operand) {
            if expression.count == 0 {
                expression.append(operand)
                return
            }
            if expression.count % 2 == 0 {
                expression.append(operand)
                return
            }
        }
    }
    mutating func addOperator(operatorToAdd: String) {
        if expression.count % 2 != 1 {
            return
        }
        switch operatorToAdd {
        case "+": expression.append(operatorToAdd)
        case "-": expression.append(operatorToAdd)
            
        default:
            return
        }
    }
    
    mutating func evaluate() -> Double? {
        var value: Double? = 0
        
        if expression.count == 0 {
            return value
        }
        
        if expression.count == 1 {
            value = Double(expression[0])
            return value
        }
        
        value = Double(expression[0])
        
        for i in stride(from: 1, to: expression.count-1, by: 2) {
            switch(expression[i]) {
            case  "+": value = value! + Double(expression[i+1])!
            case  "-": value = value! - Double(expression[i+1])!
            default: return nil
            }
        }
        
        return value
    }
    
    
    mutating func reset() {
        expression = []
    }
    
}


