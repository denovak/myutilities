//
//  NotesTableViewCell.swift
//  MyUtilities
//
//  Created by Dejan Novak on 04/04/2019.
//  Copyright © 2019 CROZ. All rights reserved.
//

import UIKit

class NotesTableViewCell: UITableViewCell {

    @IBOutlet weak var noteDate: UILabel?
    @IBOutlet weak var noteDescription: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
