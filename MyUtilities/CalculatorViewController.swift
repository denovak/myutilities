//
//  FirstViewController.swift
//  MyUtilities
//
//  Created by Dejan Novak on 03/04/2019.
//  Copyright © 2019 CROZ. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {

    var calculator = Calculator()
    var operatorMode: Bool = false
    
    @IBOutlet weak var resultLabel: UILabel?
    @IBOutlet weak var buttonSeven: UIButton?
    
    
    @IBAction func operandClicked(sender: UIButton) {
        let buttonText = sender.titleLabel?.text!
        var resultText = resultLabel?.text!
        
        if operatorMode {
          resultLabel?.text = buttonText
          operatorMode = !operatorMode
          return
        }
        
        if resultText == "0" && buttonText == "0" {
            return
        }
        if resultText == "0" {
            resultText = buttonText
        }
        else {
            resultText?.append(buttonText!)
        }
        resultLabel?.text = resultText
    }
    
    @IBAction func operatorClicked(sender: UIButton) {
        operatorMode = !operatorMode
        calculator.addOperand(operand: resultLabel!.text!)
        calculator.addOperator(operatorToAdd: sender.titleLabel!.text!)
    }
    
    @IBAction func clearClicked(sender: UIButton) {
        resultLabel?.text = "0"
        calculator.reset()
    }
    
    @IBAction func evaluationClicked(sender: UIButton) {
        let resultText = resultLabel?.text!
        
        if calculator.expression.count % 2 == 0 {
            calculator.addOperand(operand: resultText!)
            if let result = calculator.evaluate() {
                resultLabel?.text = String(Int(result))
            }
            return
        }
        if calculator.expression.count % 3 == 0 {
            if let result = calculator.evaluate() {
                resultLabel?.text = String(Int(result))
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //resultLabel?.text = "100.00"
    }


}

